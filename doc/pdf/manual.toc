\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}S3QL}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Features}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Development Status}{2}{section.1.2}%
\contentsline {section}{\numberline {1.3}Supported Platforms}{2}{section.1.3}%
\contentsline {section}{\numberline {1.4}Contributing}{2}{section.1.4}%
\contentsline {chapter}{\numberline {2}Installation}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Dependencies}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Installing S3QL}{4}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Running S3QL commands directly}{4}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Installing S3QL for the current user}{4}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Installing S3QL for all users}{5}{subsection.2.2.3}%
\contentsline {section}{\numberline {2.3}Development Version}{5}{section.2.3}%
\contentsline {section}{\numberline {2.4}Running tests requiring remote servers}{6}{section.2.4}%
\contentsline {chapter}{\numberline {3}Storage Backends}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}Google Storage}{7}{section.3.1}%
\contentsline {section}{\numberline {3.2}Amazon S3}{8}{section.3.2}%
\contentsline {section}{\numberline {3.3}OpenStack/Swift}{9}{section.3.3}%
\contentsline {section}{\numberline {3.4}Rackspace CloudFiles}{10}{section.3.4}%
\contentsline {section}{\numberline {3.5}S3 compatible}{11}{section.3.5}%
\contentsline {section}{\numberline {3.6}Backblaze B2}{12}{section.3.6}%
\contentsline {section}{\numberline {3.7}Local}{13}{section.3.7}%
\contentsline {chapter}{\numberline {4}Important Rules to Avoid Losing Data}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Rules in a Nutshell}{15}{section.4.1}%
\contentsline {section}{\numberline {4.2}Data Durability}{15}{section.4.2}%
\contentsline {chapter}{\numberline {5}File System Creation}{17}{chapter.5}%
\contentsline {chapter}{\numberline {6}Managing File Systems}{19}{chapter.6}%
\contentsline {section}{\numberline {6.1}Changing the Passphrase}{20}{section.6.1}%
\contentsline {section}{\numberline {6.2}Recovering the master key}{20}{section.6.2}%
\contentsline {section}{\numberline {6.3}Restoring Metadata Backups}{20}{section.6.3}%
\contentsline {section}{\numberline {6.4}Deleting a file system}{20}{section.6.4}%
\contentsline {section}{\numberline {6.5}Upgrading the file system}{21}{section.6.5}%
\contentsline {chapter}{\numberline {7}Mounting}{23}{chapter.7}%
\contentsline {section}{\numberline {7.1}Permission Checking}{24}{section.7.1}%
\contentsline {section}{\numberline {7.2}Compression Algorithms}{24}{section.7.2}%
\contentsline {section}{\numberline {7.3}Notes about Caching}{25}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Maximum Number of Cache Entries}{25}{subsection.7.3.1}%
\contentsline {subsection}{\numberline {7.3.2}Cache Flushing and Expiration}{25}{subsection.7.3.2}%
\contentsline {section}{\numberline {7.4}NFS Support}{25}{section.7.4}%
\contentsline {section}{\numberline {7.5}Failure Modes}{26}{section.7.5}%
\contentsline {section}{\numberline {7.6}Automatic Mounting}{26}{section.7.6}%
\contentsline {chapter}{\numberline {8}Advanced S3QL Features}{27}{chapter.8}%
\contentsline {section}{\numberline {8.1}Snapshotting and Copy\sphinxhyphen {}on\sphinxhyphen {}Write}{27}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}Snapshotting vs Hardlinking}{27}{subsection.8.1.1}%
\contentsline {section}{\numberline {8.2}Getting Statistics}{28}{section.8.2}%
\contentsline {section}{\numberline {8.3}Immutable Trees}{28}{section.8.3}%
\contentsline {section}{\numberline {8.4}Fast Recursive Removal}{29}{section.8.4}%
\contentsline {section}{\numberline {8.5}Runtime Configuration}{29}{section.8.5}%
\contentsline {chapter}{\numberline {9}Unmounting}{31}{chapter.9}%
\contentsline {chapter}{\numberline {10}Checking for Errors}{33}{chapter.10}%
\contentsline {section}{\numberline {10.1}Checking and repairing internal file system errors}{33}{section.10.1}%
\contentsline {section}{\numberline {10.2}Detecting and handling backend data corruption}{34}{section.10.2}%
\contentsline {chapter}{\numberline {11}Storing Backend Information and Credentials}{37}{chapter.11}%
\contentsline {chapter}{\numberline {12}Contributed Programs}{39}{chapter.12}%
\contentsline {section}{\numberline {12.1}benchmark.py}{39}{section.12.1}%
\contentsline {section}{\numberline {12.2}pcp.py}{39}{section.12.2}%
\contentsline {section}{\numberline {12.3}s3ql\_backup.sh}{39}{section.12.3}%
\contentsline {section}{\numberline {12.4}expire\_backups.py}{40}{section.12.4}%
\contentsline {section}{\numberline {12.5}remove\_objects.py}{41}{section.12.5}%
\contentsline {chapter}{\numberline {13}Tips \& Tricks}{43}{chapter.13}%
\contentsline {section}{\numberline {13.1}SSH Backend}{43}{section.13.1}%
\contentsline {section}{\numberline {13.2}Permanently mounted backup file system}{43}{section.13.2}%
\contentsline {section}{\numberline {13.3}Improving copy performance}{43}{section.13.3}%
\contentsline {chapter}{\numberline {14}Frequently Asked Questions}{45}{chapter.14}%
\contentsline {section}{\numberline {14.1}How can I improve the file system throughput?}{45}{section.14.1}%
\contentsline {section}{\numberline {14.2}Why does \sphinxstyleliteralintitle {\sphinxupquote {df}} show 1 TB of free space?}{45}{section.14.2}%
\contentsline {section}{\numberline {14.3}Which operating systems are supported?}{45}{section.14.3}%
\contentsline {section}{\numberline {14.4}Is there a file size limit?}{46}{section.14.4}%
\contentsline {section}{\numberline {14.5}Suppose I want to make a small change in a very large file. Will S3QL download and re\sphinxhyphen {}upload the entire file?}{46}{section.14.5}%
\contentsline {section}{\numberline {14.6}I don’t quite understand this de\sphinxhyphen {}duplication feature…}{46}{section.14.6}%
\contentsline {section}{\numberline {14.7}What does the “Transport endpoint not connected” error mean?}{46}{section.14.7}%
\contentsline {section}{\numberline {14.8}What does “Backend reports that fs is still mounted elsewhere, aborting” mean?}{46}{section.14.8}%
\contentsline {section}{\numberline {14.9}Can I access an S3QL file system on multiple computers simultaneously?}{46}{section.14.9}%
\contentsline {section}{\numberline {14.10}What block size should I use?}{47}{section.14.10}%
\contentsline {section}{\numberline {14.11}Is there a way to access the file system offline?}{47}{section.14.11}%
\contentsline {chapter}{\numberline {15}Known Issues}{49}{chapter.15}%
\contentsline {chapter}{\numberline {16}Manpages}{51}{chapter.16}%
\contentsline {section}{\numberline {16.1}The \sphinxstyleliteralstrong {\sphinxupquote {mkfs.s3ql}} command}{51}{section.16.1}%
\contentsline {subsection}{\numberline {16.1.1}Synopsis}{51}{subsection.16.1.1}%
\contentsline {subsection}{\numberline {16.1.2}Description}{51}{subsection.16.1.2}%
\contentsline {subsection}{\numberline {16.1.3}Options}{51}{subsection.16.1.3}%
\contentsline {subsection}{\numberline {16.1.4}Exit Codes}{52}{subsection.16.1.4}%
\contentsline {subsection}{\numberline {16.1.5}See Also}{53}{subsection.16.1.5}%
\contentsline {section}{\numberline {16.2}The \sphinxstyleliteralstrong {\sphinxupquote {s3qladm}} command}{53}{section.16.2}%
\contentsline {subsection}{\numberline {16.2.1}Synopsis}{53}{subsection.16.2.1}%
\contentsline {subsection}{\numberline {16.2.2}Description}{53}{subsection.16.2.2}%
\contentsline {subsection}{\numberline {16.2.3}Options}{53}{subsection.16.2.3}%
\contentsline {subsection}{\numberline {16.2.4}Actions}{54}{subsection.16.2.4}%
\contentsline {subsection}{\numberline {16.2.5}Exit Codes}{54}{subsection.16.2.5}%
\contentsline {subsection}{\numberline {16.2.6}See Also}{55}{subsection.16.2.6}%
\contentsline {section}{\numberline {16.3}The \sphinxstyleliteralstrong {\sphinxupquote {mount.s3ql}} command}{55}{section.16.3}%
\contentsline {subsection}{\numberline {16.3.1}Synopsis}{55}{subsection.16.3.1}%
\contentsline {subsection}{\numberline {16.3.2}Description}{55}{subsection.16.3.2}%
\contentsline {subsection}{\numberline {16.3.3}Options}{55}{subsection.16.3.3}%
\contentsline {subsection}{\numberline {16.3.4}Exit Codes}{56}{subsection.16.3.4}%
\contentsline {subsection}{\numberline {16.3.5}See Also}{58}{subsection.16.3.5}%
\contentsline {section}{\numberline {16.4}The \sphinxstyleliteralstrong {\sphinxupquote {s3qlstat}} command}{58}{section.16.4}%
\contentsline {subsection}{\numberline {16.4.1}Synopsis}{58}{subsection.16.4.1}%
\contentsline {subsection}{\numberline {16.4.2}Description}{58}{subsection.16.4.2}%
\contentsline {subsection}{\numberline {16.4.3}Options}{58}{subsection.16.4.3}%
\contentsline {subsection}{\numberline {16.4.4}Exit Codes}{58}{subsection.16.4.4}%
\contentsline {subsection}{\numberline {16.4.5}See Also}{59}{subsection.16.4.5}%
\contentsline {section}{\numberline {16.5}The \sphinxstyleliteralstrong {\sphinxupquote {s3qlctrl}} command}{59}{section.16.5}%
\contentsline {subsection}{\numberline {16.5.1}Synopsis}{59}{subsection.16.5.1}%
\contentsline {subsection}{\numberline {16.5.2}Description}{59}{subsection.16.5.2}%
\contentsline {subsection}{\numberline {16.5.3}Options}{60}{subsection.16.5.3}%
\contentsline {subsection}{\numberline {16.5.4}Exit Codes}{60}{subsection.16.5.4}%
\contentsline {subsection}{\numberline {16.5.5}See Also}{60}{subsection.16.5.5}%
\contentsline {section}{\numberline {16.6}The \sphinxstyleliteralstrong {\sphinxupquote {s3qlcp}} command}{60}{section.16.6}%
\contentsline {subsection}{\numberline {16.6.1}Synopsis}{60}{subsection.16.6.1}%
\contentsline {subsection}{\numberline {16.6.2}Description}{61}{subsection.16.6.2}%
\contentsline {subsubsection}{Snapshotting vs Hardlinking}{61}{subsubsection*.65}%
\contentsline {subsection}{\numberline {16.6.3}Options}{61}{subsection.16.6.3}%
\contentsline {subsection}{\numberline {16.6.4}Exit Codes}{62}{subsection.16.6.4}%
\contentsline {subsection}{\numberline {16.6.5}See Also}{62}{subsection.16.6.5}%
\contentsline {section}{\numberline {16.7}The \sphinxstyleliteralstrong {\sphinxupquote {s3qlrm}} command}{62}{section.16.7}%
\contentsline {subsection}{\numberline {16.7.1}Synopsis}{62}{subsection.16.7.1}%
\contentsline {subsection}{\numberline {16.7.2}Description}{62}{subsection.16.7.2}%
\contentsline {subsection}{\numberline {16.7.3}Options}{62}{subsection.16.7.3}%
\contentsline {subsection}{\numberline {16.7.4}Exit Codes}{63}{subsection.16.7.4}%
\contentsline {subsection}{\numberline {16.7.5}See Also}{63}{subsection.16.7.5}%
\contentsline {section}{\numberline {16.8}The \sphinxstyleliteralstrong {\sphinxupquote {s3qllock}} command}{63}{section.16.8}%
\contentsline {subsection}{\numberline {16.8.1}Synopsis}{63}{subsection.16.8.1}%
\contentsline {subsection}{\numberline {16.8.2}Description}{63}{subsection.16.8.2}%
\contentsline {subsection}{\numberline {16.8.3}Rationale}{63}{subsection.16.8.3}%
\contentsline {subsection}{\numberline {16.8.4}Options}{64}{subsection.16.8.4}%
\contentsline {subsection}{\numberline {16.8.5}Exit Codes}{64}{subsection.16.8.5}%
\contentsline {subsection}{\numberline {16.8.6}See Also}{64}{subsection.16.8.6}%
\contentsline {section}{\numberline {16.9}The \sphinxstyleliteralstrong {\sphinxupquote {umount.s3ql}} command}{65}{section.16.9}%
\contentsline {subsection}{\numberline {16.9.1}Synopsis}{65}{subsection.16.9.1}%
\contentsline {subsection}{\numberline {16.9.2}Description}{65}{subsection.16.9.2}%
\contentsline {subsection}{\numberline {16.9.3}Options}{65}{subsection.16.9.3}%
\contentsline {subsection}{\numberline {16.9.4}Exit Codes}{65}{subsection.16.9.4}%
\contentsline {subsection}{\numberline {16.9.5}See Also}{66}{subsection.16.9.5}%
\contentsline {section}{\numberline {16.10}The \sphinxstyleliteralstrong {\sphinxupquote {fsck.s3ql}} command}{66}{section.16.10}%
\contentsline {subsection}{\numberline {16.10.1}Synopsis}{66}{subsection.16.10.1}%
\contentsline {subsection}{\numberline {16.10.2}Description}{66}{subsection.16.10.2}%
\contentsline {subsection}{\numberline {16.10.3}Options}{66}{subsection.16.10.3}%
\contentsline {subsection}{\numberline {16.10.4}Exit Codes}{67}{subsection.16.10.4}%
\contentsline {subsection}{\numberline {16.10.5}See Also}{68}{subsection.16.10.5}%
\contentsline {section}{\numberline {16.11}The \sphinxstyleliteralstrong {\sphinxupquote {s3ql\_oauth\_client}} command}{68}{section.16.11}%
\contentsline {subsection}{\numberline {16.11.1}Synopsis}{68}{subsection.16.11.1}%
\contentsline {subsection}{\numberline {16.11.2}Description}{68}{subsection.16.11.2}%
\contentsline {subsection}{\numberline {16.11.3}Options}{68}{subsection.16.11.3}%
\contentsline {subsection}{\numberline {16.11.4}Exit Codes}{69}{subsection.16.11.4}%
\contentsline {subsection}{\numberline {16.11.5}See Also}{69}{subsection.16.11.5}%
\contentsline {section}{\numberline {16.12}The \sphinxstyleliteralstrong {\sphinxupquote {s3ql\_verify}} command}{69}{section.16.12}%
\contentsline {subsection}{\numberline {16.12.1}Synopsis}{69}{subsection.16.12.1}%
\contentsline {subsection}{\numberline {16.12.2}Description}{69}{subsection.16.12.2}%
\contentsline {subsection}{\numberline {16.12.3}Options}{70}{subsection.16.12.3}%
\contentsline {subsection}{\numberline {16.12.4}Exit Codes}{70}{subsection.16.12.4}%
\contentsline {subsection}{\numberline {16.12.5}See Also}{71}{subsection.16.12.5}%
\contentsline {section}{\numberline {16.13}The \sphinxstyleliteralstrong {\sphinxupquote {pcp}} command}{71}{section.16.13}%
\contentsline {subsection}{\numberline {16.13.1}Synopsis}{71}{subsection.16.13.1}%
\contentsline {subsection}{\numberline {16.13.2}Description}{72}{subsection.16.13.2}%
\contentsline {subsection}{\numberline {16.13.3}Options}{72}{subsection.16.13.3}%
\contentsline {subsection}{\numberline {16.13.4}Exit Codes}{72}{subsection.16.13.4}%
\contentsline {subsection}{\numberline {16.13.5}See Also}{72}{subsection.16.13.5}%
\contentsline {section}{\numberline {16.14}The \sphinxstyleliteralstrong {\sphinxupquote {expire\_backups}} command}{73}{section.16.14}%
\contentsline {subsection}{\numberline {16.14.1}Synopsis}{73}{subsection.16.14.1}%
\contentsline {subsection}{\numberline {16.14.2}Description}{73}{subsection.16.14.2}%
\contentsline {subsection}{\numberline {16.14.3}Options}{74}{subsection.16.14.3}%
\contentsline {subsection}{\numberline {16.14.4}Exit Codes}{74}{subsection.16.14.4}%
\contentsline {subsection}{\numberline {16.14.5}See Also}{74}{subsection.16.14.5}%
\contentsline {chapter}{\numberline {17}Further Resources / Getting Help}{75}{chapter.17}%
\contentsline {chapter}{\numberline {18}Implementation Details}{77}{chapter.18}%
\contentsline {section}{\numberline {18.1}Metadata Storage}{77}{section.18.1}%
\contentsline {section}{\numberline {18.2}Data Storage}{77}{section.18.2}%
\contentsline {section}{\numberline {18.3}Data De\sphinxhyphen {}Duplication}{78}{section.18.3}%
\contentsline {section}{\numberline {18.4}Caching}{78}{section.18.4}%
\contentsline {section}{\numberline {18.5}Encryption}{78}{section.18.5}%
\contentsline {chapter}{Index}{79}{section*.66}%
